package travel.lodge.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import travel.lodge.application.model.Hotel;
import travel.lodge.application.service.HotelService;

@RestController
public class HotelController<JSONObject> {

	@Autowired
	private HotelService service;

	/**
	 * 
	 * This method is related to add a hotel into Travel Logde System
	 * 
	 */
	@PostMapping("add/hotel")
	public ResponseEntity<Object> addHotel(@RequestBody Hotel hotel) {
		hotel = service.saveHotel(hotel);
		return new ResponseEntity<Object>("Hotel is created successfully with Id = " + hotel.getId(),
				HttpStatus.CREATED);
	}

	/**
	 * 
	 * This method is related to add list of hotel into Travel Logde System
	 * 
	 */
	@PostMapping("add/hotels")
	public List<Hotel> addHotels(@RequestBody List<Hotel> hotels) {
		return service.saveHotels(hotels);
	}

	/**
	 * 
	 * This method is related to get list of hotel from the Travel Logde System
	 * 
	 */
	@GetMapping({ "get/hotel/all" })
	public List<Hotel> findAllHotels() {
		return service.getHotels();
	}

	/**
	 * 
	 * This method is related to get a hotel by using hotel id from the Travel Logde
	 * System
	 * 
	 */
	@GetMapping("get/hotel/{id}")
	public Hotel findHotelById(@PathVariable int id) {
		return service.getHotelById(id);
	}

	/**
	 * 
	 * This method is related to get a hotel by using hotel name from the Travel
	 * Logde System
	 * 
	 */
	@GetMapping("get/hotel/{name}")
	public Hotel findHotelByName(@PathVariable String name) {
		return service.getHotelByName(name);
	}

	/**
	 * 
	 * This method is related to update a hotel details in the Travel Logde System
	 * 
	 */
	@PutMapping("update/hotel")
	public Hotel updateHotel(@RequestBody Hotel hotel) {
		return service.updateHotel(hotel);
	}

	/**
	 * 
	 * This method is related to get a hotel by using hotel id from the Travel Logde
	 * System
	 * 
	 */
	@DeleteMapping("/deleteHotel/{id}")
	public String deleteHotel(@PathVariable int id) {
		return service.deleteHotel(id);
	}

}
