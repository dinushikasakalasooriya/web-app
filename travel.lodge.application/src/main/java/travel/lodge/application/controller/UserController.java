package travel.lodge.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import travel.lodge.application.model.User;
import travel.lodge.application.repository.UserRepository;

@Controller
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	/**
	 * 
	 * This is related to user login loading page of 
	 * the hotel system 
	 * 
	 * */	
	@RequestMapping({"secure/login"})
	public String login() {
		return "userlogin";
	}
	
	/**
	 * 
	 * This is related to user registration loading of 
	 * the hotel system 
	 * 
	 * */
	@RequestMapping("signup")
	public String signup() {
		return "signup";
	}

	/**
	 * 
	 * This is related to user profile of 
	 * the hotel system 
	 * 
	 * */
	@RequestMapping("secure/user/account")
	public String userAccount() {
		return "useraccount";
	}

	/**
	 * 
	 * This is related to user registration of 
	 * the hotel system 
	 * 
	 * */
	@PostMapping("user/register")
	public String userAdd(@RequestBody User user) {
		System.out.println(user);
		userRepository.save(user);
		return "Hey " + user.getUsername() + " Your registration procss is completed successfully.";
	}

	/**
	 * 
	 * This is related to user account details display view for authenticated user 
	 * the hotel system .
	 * 
	 * */
	@RequestMapping("secure/user")
	  public String currentUserName() {
		String username;
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (principal instanceof UserDetails) {
				 username = ((UserDetails)principal).getUsername();
			} else {
				 username = principal.toString();
				 
				 
				 User user = null;
				 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				 if (authentication != null
				    && authentication.getPrincipal() != null
				    && authentication.getPrincipal() instanceof User) {
				         user = (User) authentication.getPrincipal();
				     }
				 System.out.println("user:"+user); 
			}
			System.out.println("username:"+username);
			
	     return username;
	  }

	/**
	 * This is related to user registration of 
	 * the hotel system.
	 * In this place, we can implement by getting Json object, but I got an error &
	 * take time to troubleshoot. So I applied below logic for that
	 */
	@PostMapping("users/add")
	public String usersAdd(@RequestParam("fullname") String fullname, @RequestParam("username") String username,
			@RequestParam("password") String password, @RequestParam("email") String email,
			@RequestParam("address") String address, @RequestParam("phone") String phone,
			@RequestParam("city") String city, @RequestParam("country") String country) throws Exception {

		String encryptPwd = passwordEncoder.encode(password);
		User user = new User();
		user.setFullname(fullname);
		user.setUsername(username);
		user.setEmail(email);
		user.setPassword(encryptPwd); // Need to apply encryption like above signupAdminUser method
		user.setAddress(address);
		user.setCity(city);
		user.setCountry(country);
		user.setPhone(Integer.parseInt(phone));
		userRepository.save(user);
		return "Hey! "+ username+" Your Acount Is Created Successfully";
	}

}
