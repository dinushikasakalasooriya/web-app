package travel.lodge.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import travel.lodge.application.model.User;
import travel.lodge.application.repository.UserRepository;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class AdminController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	/**
	 * 
	 * This is related to add admin into 
	 * the hotel system for the manage hotel details 
	 * and system related things
	 * 
	 * */	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping("admin/add")
	public String signupAdminUser(@RequestBody User user) {
		String pwd = user.getPassword();
		String encryptPwd = passwordEncoder.encode(pwd);
		user.setPassword(encryptPwd);
		System.out.println(user);
		userRepository.save(user);
		return "Admin add sussessfully....";
	}


}
