package travel.lodge.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import travel.lodge.application.model.Room;
import travel.lodge.application.service.RoomService;

@RestController
public class RoomController {
	
	@Autowired
	private RoomService service;
	
	/**
	 * 
	 * This method is related to get list of rooms
	 * from the Travel Logde System
	 * 
	 */
	@GetMapping("get/room/all")
	public Room findAllHotels(String name) {
		return service.getRoom(name);
	}
}
