package travel.lodge.application.controller;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import travel.lodge.application.model.Reservation;
import travel.lodge.application.service.ReservationService;

@RestController
public class ReservationController {
	
	@Autowired
	private ReservationService service;
	
	/**
	 * 
	 * This method is related to get a hotel reservation by using 
	 * reservation id from the Travel Logde System
	 * 
	 */
	@GetMapping("get/reservation/{id}")	
	public Optional<Reservation> getReservation(int id) {
		return service.getReservation(id);
	}
}
