package travel.lodge.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import travel.lodge.application.service.HotelService;
import travel.lodge.application.service.RoomService;

@Controller
public class IndexController {
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private RoomService roomService;	
	
	/**
	 * 
	 * This method is related to index page loading
	 * 
	 * */
	@RequestMapping("index")
	public String index() {
		return "index";
	}

	/**
	 * 
	 * This method is related to index page Hotel list/ Room list loading process
	 * 
	 * */
	@GetMapping({"/"})
	public String findAllHotels(ModelMap modelMap){
		modelMap.put("listHotel", hotelService.getHotels());
		modelMap.put("listRooms", roomService.getRooms());
		return "index";		
	}
}
