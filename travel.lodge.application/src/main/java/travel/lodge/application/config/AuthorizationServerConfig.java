package travel.lodge.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

import travel.lodge.application.service.CustomUserDetailsService;

/**
 * This is the AuthorizationServerConfig and class configures the authorization server.
 */
//@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired(required = true)
	@Qualifier("customUserDetailsService")
	private CustomUserDetailsService customUserDetailsService;

    /**
     * defines the security constraints on the token end point.
     * & it up to isAuthenticated
     */
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");

	}

    /**
     * 
     * Setting up the clients with a clientId, a clientSecret, a scope, the grant types and the authorities.
     * 
     */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory().withClient("ClientId").secret("secret").authorizedGrantTypes("authorization_code")
				.scopes("user").autoApprove(true);
	}

	 /**
     * Setting up the endpointsconfigurer authentication manager.
     */
	@Qualifier("authenticationManagerBean")
	@Override
	@Bean
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(this.authenticationManager);
		endpoints.userDetailsService(this.customUserDetailsService);
	}

}
