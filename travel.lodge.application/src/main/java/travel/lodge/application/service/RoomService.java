package travel.lodge.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.Room;
import travel.lodge.application.repository.RoomRepository;

@Service
public class RoomService {
	
	@Autowired
	private RoomRepository roomRepository;
		
	public Room getRoom(String name) {
		return roomRepository.findByname(name);
	}

	public List<Room> getRooms() {
		return roomRepository.findAll();
	}
}
