package travel.lodge.application.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.Reservation;
import travel.lodge.application.repository.ReservationRepository;

@Service
public class ReservationService {
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	public Optional<Reservation> getReservation(int id) {
		return reservationRepository.findById(id);
	} 

}
