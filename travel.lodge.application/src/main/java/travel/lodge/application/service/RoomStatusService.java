package travel.lodge.application.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.RoomStatus;
import travel.lodge.application.repository.RoomStatusRepository;

@Service
public class RoomStatusService {
	
	@Autowired
	private RoomStatusRepository roomStatusRepository;
	
	public Optional<RoomStatus> getStatus(int roomId) {
		return roomStatusRepository.findById(roomId);
	}

}
