package travel.lodge.application.service;

import java.net.UnknownServiceException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.User;
import travel.lodge.application.repository.UserRepository;
@Service
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		
		Optional<User> userOptional= userRepository.findByUsername(username);
		try {
			userOptional
			.orElseThrow(()-> new UnknownServiceException("User Name is not found"));
		} catch (UnknownServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userOptional.map(CustomUserDetails::new).get();
	
	}
}
