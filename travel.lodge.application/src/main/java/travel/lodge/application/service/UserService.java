package travel.lodge.application.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.User;
import travel.lodge.application.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository repository;

	public Optional<User> getUserByName(String name) {
		return repository.findByUsername(name);
	}

}
