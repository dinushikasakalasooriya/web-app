package travel.lodge.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import travel.lodge.application.model.Hotel;
import travel.lodge.application.repository.HotelRepository;

@Service
public class HotelService {
	@Autowired
	private HotelRepository repository;

	public Hotel saveHotel(Hotel hotel) {
		return repository.save(hotel);
	}

	public List<Hotel> saveHotels(List<Hotel> hotels) {
		return repository.saveAll(hotels);
	}

	public List<Hotel> getHotels() {
		return repository.findAll();
	}

	public Hotel getHotelById(int id) {
		return repository.findById(id).orElse(null);
	}

	public Hotel getHotelByName(String name) {
		return repository.findByName(name);
	}

	public String deleteHotel(int id) {
		repository.deleteById(id);
		return "Hotel is removed from the list || Hotel ID is :" + id;
	}

	public Hotel updateHotel(Hotel hotel) {
		Hotel existingHotel = repository.findById(hotel.getId()).orElse(hotel);
		existingHotel.setName(hotel.getName());
		existingHotel.setLocation(hotel.getLocation());
		existingHotel.setHoteltypes(hotel.getHoteltypes());
		existingHotel.setDiscripltion(hotel.getDiscripltion());
		existingHotel.setRooms_quantity(hotel.getRooms_quantity());
		return repository.save(existingHotel);
	}

}
