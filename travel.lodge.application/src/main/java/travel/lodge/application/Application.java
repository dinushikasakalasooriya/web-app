package travel.lodge.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })

/* Travel Lodge Web Application */
/**
 * This is the booking application based on Travel Lodge System in UK
 * 
 * It is mainly based on oauth2 security with user authentication & user
 * registration.
 * 
 * Other than that the REST API provide several CURD operations including search
 * operations related to the Travel Lodge System.
 */
public class Application {

	/* Main Class of the Travel Lodge Web Application */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		System.out.println("Travel Lodge System Is Running Successfully...! ");
	}
}
