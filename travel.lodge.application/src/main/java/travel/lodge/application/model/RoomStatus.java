package travel.lodge.application.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "room_status")
public class RoomStatus {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "room_status_id")
	private int id;

	@Column(name = "room_status")
	private String status;

	@OneToMany(mappedBy = "roomStatus", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Room> rooms;

	public RoomStatus() {
		rooms = new HashSet<>();
	}

	public RoomStatus(String status) {
		this.status = status;
		rooms = new HashSet<>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Set<Room> rooms) {
		this.rooms = rooms;
		for (Room room : rooms) {
			room.setRoomStatus(this);
		}
	}
}
