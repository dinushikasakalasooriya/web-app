package travel.lodge.application.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "room_type")
public class RoomType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "room_type_id")
	private int id;

	@Column(name = "room_type")
	private String type;

	@OneToMany(mappedBy = "roomType", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Room> rooms;

	public RoomType() {
		rooms = new HashSet<>();
	}

	public RoomType(String type) {
		this.type = type;
		rooms = new HashSet<>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Set<Room> rooms) {
		this.rooms = rooms;
		for (Room room : rooms) {
			room.setRoomType(this);
		}
	}
}