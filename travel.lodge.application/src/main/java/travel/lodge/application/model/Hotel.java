package travel.lodge.application.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "hotel")
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hotel_id")
	private int id;

	@Column(name = "hotel_name")
	private String name;

	@Column(name = "hotel_location")
	private String location;

	@Column(name = "discripltion")
	private String discripltion;

	@Column(name = "hotel_room_quantity")
	private String rooms_quantity;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "hotel_type", joinColumns = @JoinColumn(name = "id"), inverseJoinColumns = @JoinColumn(name = "hotel_type_id"))
	private Set<HotelTypeDetails> hoteltypes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDiscripltion() {
		return discripltion;
	}

	public void setDiscripltion(String discripltion) {
		this.discripltion = discripltion;
	}

	public String getRooms_quantity() {
		return rooms_quantity;
	}

	public void setRooms_quantity(String rooms_quantity) {
		this.rooms_quantity = rooms_quantity;
	}

	public Set<HotelTypeDetails> getHoteltypes() {
		return hoteltypes;
	}

	public void setHoteltypes(Set<HotelTypeDetails> hoteltypes) {
		this.hoteltypes = hoteltypes;
	}

}
