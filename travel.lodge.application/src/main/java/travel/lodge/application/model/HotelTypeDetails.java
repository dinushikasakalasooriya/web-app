package travel.lodge.application.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "totel_type_details")
public class HotelTypeDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hotel_type_id")
	private int hotel_type_id;

	@Column(name = "hotel_type_name")
	private String hotel_type_name;

	public int getHotel_type_id() {
		return hotel_type_id;
	}

	public void setHotel_type_id(int hotel_type_id) {
		this.hotel_type_id = hotel_type_id;
	}

	public String gethotel_type_name() {
		return hotel_type_name;
	}

	public void sethotel_type_name(String hotel_name) {
		this.hotel_type_name = hotel_name;
	}

}
