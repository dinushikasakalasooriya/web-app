package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.Room;

public interface RoomRepository extends JpaRepository<Room,Integer>{

	Room findByname(String name);

}
