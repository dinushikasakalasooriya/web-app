package travel.lodge.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	Optional<User> findByUsername(String username);

}
