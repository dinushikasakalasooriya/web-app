package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation,Integer>{

}
