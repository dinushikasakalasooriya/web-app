package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
