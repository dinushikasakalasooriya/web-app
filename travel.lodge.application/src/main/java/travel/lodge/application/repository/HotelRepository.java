package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, Integer> {

	Hotel findByName(String name);

}
