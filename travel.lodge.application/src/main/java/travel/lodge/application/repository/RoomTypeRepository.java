package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.RoomType;

public interface RoomTypeRepository extends JpaRepository<RoomType,Integer>  {

}
