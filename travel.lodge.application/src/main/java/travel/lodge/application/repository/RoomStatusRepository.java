package travel.lodge.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import travel.lodge.application.model.RoomStatus;

public interface RoomStatusRepository extends JpaRepository<RoomStatus,Integer>{

}
