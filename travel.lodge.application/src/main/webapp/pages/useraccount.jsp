<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	background-color: black;
}

* {
	box-sizing: border-box;
}

/* Add padding to containers */
.container {
	padding: 16px;
	background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
	width: 100%;
	padding: 15px;
	margin: 5px 0 22px 0;
	display: inline-block;
	border: none;
	background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
	background-color: #ddd;
	outline: none;
}

/* Overwrite default styles of hr */
hr {
	border: 1px solid #f1f1f1;
	margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
	background-color: #4CAF50;
	color: white;
	padding: 16px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
	opacity: 0.9;
}

.registerbtn:hover {
	opacity: 1;
}

/* Add a blue text color to links */
a {
	color: dodgerblue;
}
/* Set a style for all buttons */
button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
	background-color: #f1f1f1;
	text-align: center;
}
</style>
</head>
<body>
${user}
	<form action="secure/user/account" method="Post">
		<div class="container">
			<h1>Your Travel Logde Account</h1>
			<p>Personal Information.</p>
			<hr>
			<label for="fullname" ><b>Username</b></label> <input type="text"
				placeholder="Enter User Name" name="fullname" id="fullname" required>

			<label for="password"><b>Password</b></label> <input type="password"
				placeholder="Enter Password" name="password" id="password" required> 
			
			<label for="password"><b>Confirm Password</b></label> <input type="password"
				placeholder="Enter Your Confirm Password" name="password" id="password" required>
				
			<label for="fullname" ><b>Full Name</b></label> <input type="text"
				placeholder="Enter Full Name" name="fullname" id="fullname" required>

			<label for="address"><b>Phone</b></label> <input type="text"
				placeholder="Enter Contact No" name="phone" id="phone" required> 
			
			<label for="address"><b>Address</b></label> <input type="text"
				placeholder="Enter Your Address" name="address" id="address" required>				
			
			<label for="city"><b>City</b></label> <input type="text"
				placeholder="Enter Your City" name="city" id="city" required>
				
			<label for="country"><b>Country</b></label> <input type="text"
				placeholder="Enter Your Country" name="country" id="country" required>
			
			<p>
				By creating an account you agree to our <a href="#">Terms &
					Privacy</a>.
			</p>

			<button type="submit" class="/index">
				<h3>Log out</h3>
			</button>
		</div>

		<div class="container signin">
			<p>
				Go Back To Home? <a href="/index">Home</a>.
			</p>
		</div>
	</form>

</body>
</html>
