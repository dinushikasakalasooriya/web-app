<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<curl-H'Content-Type:application/json'>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="signup/images/icons/favicon.ico" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="signup/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="signup/css/util.css">
<link rel="stylesheet" type="text/css" href="signup/css/main.css">
<!--===============================================================================================-->

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#signupbutton').click(function() {
			$.ajax({
				type : "post",
				url : "localhost:8080/user/add", //this is my servlet
				data : "input=" + $('#ip').val() + "&output=" + $('#op').val(),
				success : function(msg) {
					$('#output').append(msg);
				}
			});
		});

	});
	$(document).on('click', '#signupbutton', function() {
		var url = 'http://localhost:8080/user/add';

		$.ajax({
			url : url,
			dataType : "json",
			async : true,
			type : "POST",
			headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},

			data : JSON.stringify($("#form_register").serializeObject()),
			success : function(result) {
				alert("Success");
			},
			error : function(request, error) {
				alert('Network error has occurred please try again!' + error);
			}
		});
	});
</script>
</head>
<body style="background-color: #999999;">
	<script src="js/form.js"></script>

	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more"
				style="background-image: url('images/bg-01.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" id="form_register"
					action="<%=request.getContextPath()%>secure/login" method="post">
					<span class="login100-form-title p-b-59"> Sign Up </span>

					<div class="form-group">
						<label for="Name"><span class="req">* </span> User Name: <small>This
								will be your login user name</small> </label> <input required type="text"
							name="username" id="username" class="form-control phone"
							maxlength="28" placeholder="Enter your fullname..." />
					</div>

					<div class="form-group">
						<label for="Email"><span class="req">* </span> Email: </label> <input
							class="form-control" type="email" name="email" id="email"
							required placeholder="Enter valid email address" />
						<div id=""></div>
					</div>

					<div class="form-group">
						<label for="Username"><span class="req">* </span> Full
							Name : </label> <input class="form-control" type="text" name="fullname"
							id="fullname" placeholder="Enter Your Full Name Here..." required />
						<div id=""></div>
					</div>

					<div class="form-group">
						<label for="Password"><span class="req">* </span>
							Password: </label> <input class="form-control" required type="password"
							name="password" id="password"
							placeholder="Enter Your Password Here..." />
						<div class="" id=""></div>
					</div>

					<div class="form-group">
						<label for="Password"><span class="req">* </span> Confirm
							Password: </label> <input class="form-control" required type="password"
							id="password" placeholder="Enter Your Comfirm Password Here..." />
						<div class="" id=""></div>
					</div>
					<div class="form-group">
						<label for="Name"><span class="req">* </span> Contact No:
							<small></small> </label> <input required type="text" name="phone"
							id="phone" class="form-control phone" maxlength="28"
							placeholder="Enter your Contact No Here" />
					</div>

					<div class="form-group">
						<label for="Name"><span class="req">* </span> Address : <small></small>
						</label> <input required type="text" name="address" id="address"
							class="form-control phone" maxlength="28"
							placeholder="Enter your Address Here" />
					</div>

					<div class="form-group">
						<label for="Name"><span class="req">* </span> City : <small></small>
						</label> <input required type="text" name="city" id="city"
							class="form-control phone" maxlength="28"
							placeholder="Enter your City Here" />
					</div>
					<div class="form-group">
						<label for="Name"><span class="req">* </span> Country : <small></small>
						</label> <input required type="text" name="country" id="country"
							class="form-control phone" maxlength="28"
							placeholder="Enter your City Here" />
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" id="signupbutton"
								onclick="userAdd()">Sign Up</button>

							<script>
								function userAdd() {
									alert("Are you confirm your account details? ");
								}
							</script>

						</div>

						<a href="secure/login"
							class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30"> Sign
							in <i class="fa fa-long-arrow-right m-l-5"></i>
						</a> <a href="index"
							class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30"> Or
							Go back to Home Page <i class="fa fa-long-arrow-right m-l-5"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--===============================================================================================-->
	<script src="signup/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="signup/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="signup/vendor/bootstrap/js/popper.js"></script>
	<script src="signup/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="signup/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="signup/vendor/daterangepicker/moment.min.js"></script>
	<script src="signup/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="signup/vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="signup/js/main.js"></script>

</body>
</html>